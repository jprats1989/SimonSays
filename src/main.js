import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import Play from './components/Play.vue'
import FormNumberColors from './components/FormNumberColors.vue'

Vue.use(VueRouter);

var router = new VueRouter({
    routes:[
        {
            path: '/',
            component: FormNumberColors
        },
        {
            path: '/play/:numberColors',
            component: Play
        }
    ]
});


var vm = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
